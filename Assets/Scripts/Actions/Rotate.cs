using App.GameName.Components;
using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.GameName.Actions
{
    [NodeGraphGroupName("Examples")]
    public class Rotate : CoreAction
    {

        public InputComponent<Angle> angle;
        public InputComponent<Center> center;
        public InputComponent<Direction> direction;
        [InjectComponent] public Rigidbody _rigidbody;

        protected override bool Action()
        {
            _rigidbody.transform.transform.RotateAround(
               center.Component.CenterValue, direction.Component.DirectionValue,
                angle.Component.AngleValue * Time.deltaTime);

            return true;
        }
    }
}