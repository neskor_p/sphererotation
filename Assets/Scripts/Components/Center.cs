using Krem.AppCore;
using Krem.AppCore.Attributes;
using UnityEngine;

namespace App.GameName.Components
{
    [NodeGraphGroupName("Examples")]
    [DisallowMultipleComponent]
    public class Center : CoreComponent
    {
        [SerializeField] private Vector3 _center;
        public Vector3 CenterValue
        {
            get => _center;
            set
            {
                _center = value;
            }
        }
    }
}