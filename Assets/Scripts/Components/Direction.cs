using Krem.AppCore;
using Krem.AppCore.Attributes;
using UnityEngine;

namespace App.GameName.Components
{
    [NodeGraphGroupName("Examples")]
    [DisallowMultipleComponent]
    public class Direction : CoreComponent
    {
        [SerializeField] private Vector3 _direction;

        public Vector3 DirectionValue
        {
            get => _direction;
            set
            {
                _direction = value;
            }
        }
    }
}