using Krem.AppCore;
using Krem.AppCore.Attributes;
using UnityEngine;

namespace App.GameName.Components
{
    [NodeGraphGroupName("Examples")]
    [DisallowMultipleComponent]
    public class Angle : CoreComponent
    {
        [SerializeField] private float _angle;

        public float AngleValue
        {
            get => _angle;
            set
            {
                if (value < 0)
                {
                    return;
                }

                _angle = value;
            }
        }
    }
}