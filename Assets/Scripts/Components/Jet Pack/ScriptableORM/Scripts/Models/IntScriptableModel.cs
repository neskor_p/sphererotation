using UnityEngine;

namespace Krem.AppCore.ScriptableORM.Models
{
    [CreateAssetMenu(fileName = "IntScriptableModel", menuName = "ScriptableORM/IntScriptableModel", order = 0)]
    public class IntScriptableModel : ScriptableModel<IntModel> 
    {
    }
}