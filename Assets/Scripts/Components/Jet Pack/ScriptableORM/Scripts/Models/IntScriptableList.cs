using UnityEngine;

namespace Krem.AppCore.ScriptableORM.Models
{
    [CreateAssetMenu(fileName = "IntScriptableList", menuName = "ScriptableORM/IntScriptableList", order = 0)]
    public class IntScriptableList : ScriptableList<IntModel>
    {
    }
}
