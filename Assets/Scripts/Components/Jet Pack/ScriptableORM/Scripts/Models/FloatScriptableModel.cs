using UnityEngine;

namespace Krem.AppCore.ScriptableORM.Models
{
    [CreateAssetMenu(fileName = "FloatScriptableModel", menuName = "ScriptableORM/FloatScriptableModel", order = 0)]
    public class FloatScriptableModel : ScriptableModel<StringModel>
    {
    }
}