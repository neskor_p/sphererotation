using UnityEngine;

namespace Krem.AppCore.ScriptableORM.Models
{
    [CreateAssetMenu(fileName = "FloatScriptableList", menuName = "ScriptableORM/FloatScriptableList", order = 0)]
    public class FloatScriptableList : ScriptableList<FloatModel>
    {
    }
}
