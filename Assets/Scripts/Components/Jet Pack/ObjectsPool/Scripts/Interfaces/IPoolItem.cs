namespace Krem.AppCore.ObjectsPool.Interfaces
{
    public interface IPoolItem
    {
        public void ReturnToPool();
    }
}