﻿using Krem.AppCore.Attributes;
using Krem.AppCore.Basis.Components;
using Krem.AppCore.Ports;
using UnityEngine.SceneManagement;

namespace Krem.AppCore.Basis.Actions
{
    [NodeGraphGroupName("Jet Pack/Basis/SceneComponent")]
    public class LoadScene : CoreAction
    {
        public InputComponent<SceneComponent> SceneComponent;
        
        protected override bool Action()
        {
            if (SceneComponent.Component.reloadCurrentScene)
            {
                Scene currentScene = SceneManager.GetActiveScene();
                SceneManager.LoadScene(currentScene.name);
                
                return true;
            }
            
            SceneManager.LoadScene(SceneComponent.Component.sceneName);
            
            return true;
        }
    }
}
