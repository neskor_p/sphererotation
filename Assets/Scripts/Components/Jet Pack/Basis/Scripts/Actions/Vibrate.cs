﻿using Krem.AppCore.Attributes;
using UnityEngine;

namespace Krem.AppCore.Basis.Actions
{
    [NodeGraphGroupName("Jet Pack/Basis")]
    public class Vibrate : CoreAction 
    {
        protected override bool Action()
        {
            Handheld.Vibrate();
            
            return true;
        }
    }
}