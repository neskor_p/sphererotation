﻿using Krem.AppCore.Attributes;
using UnityEngine;

namespace Krem.AppCore.Basis.Actions
{
    [NodeGraphGroupName("Jet Pack/Basis/GameObject")]
    public class DestroyGameObject : CoreAction
    {
        [InjectComponent] private Transform _transform;
        
        [ActionParameter] public bool DestroyImmediate = false;
        
        protected override bool Action()
        {
            if (DestroyImmediate)
            {
                UnityEngine.GameObject.DestroyImmediate(_transform.gameObject);
            }
            else
            {
                UnityEngine.GameObject.Destroy(_transform.gameObject);
            }
            
            return true;
        }
    }
}