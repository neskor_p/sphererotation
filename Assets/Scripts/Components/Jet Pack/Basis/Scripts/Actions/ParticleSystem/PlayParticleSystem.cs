using Krem.AppCore.Attributes;

namespace Krem.AppCore.Basis.Actions
{
    [NodeGraphGroupName("Jet Pack/Basis/ParticleSystem")] 
    public class PlayParticleSystem : CoreAction
    {
        [InjectComponent] private UnityEngine.ParticleSystem _particleSystem;
        
        protected override bool Action()
        {
            _particleSystem.Play();
        
            return true;
        }
    }
}