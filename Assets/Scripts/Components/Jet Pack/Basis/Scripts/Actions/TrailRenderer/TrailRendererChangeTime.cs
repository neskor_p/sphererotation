﻿using Krem.AppCore.Attributes;
using UnityEngine;

namespace Krem.AppCore.Basis.Actions
{
    [NodeGraphGroupName("Jet Pack/Basis/TrailRenderer")]
    public class TrailRendererChangeTime : CoreAction
    {
        [InjectComponent] private TrailRenderer _trailRenderer;
        
        [ActionParameter] public float time = 1f;
        
        protected override bool Action()
        {
            _trailRenderer.time = time;
            
            return true;
        }
    }
}