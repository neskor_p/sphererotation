﻿using Krem.AppCore.Attributes;
using UnityEngine;

namespace Krem.AppCore.Basis.Actions
{
    [NodeGraphGroupName("Jet Pack/Basis/TrailRenderer")]
    public class TrailRendererClear : CoreAction
    {
        [InjectComponent] private TrailRenderer _trailRenderer;
        
        protected override bool Action()
        {
            _trailRenderer.Clear();
            
            return true;
        }
    }
}