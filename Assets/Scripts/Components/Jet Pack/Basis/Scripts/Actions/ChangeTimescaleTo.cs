using Krem.AppCore.Attributes;
using UnityEngine;

namespace Krem.AppCore.Basis.Actions
{
    [NodeGraphGroupName("Jet Pack/Basis")] 
    public class ChangeTimescaleTo : CoreAction
    {
        [ActionParameter] public float timeScale = 1f;
        
        protected override bool Action()
        {
            Time.timeScale = timeScale;
        
            return true;
        }
    }
}