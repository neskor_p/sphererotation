using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace Krem.AppCore.Basis.Components.Handlers
{
    [NodeGraphGroupName("Jet Pack/Basis/Handlers")]
    public class StartHandler : CoreComponent
    {
        [Header("Ports")]
        public OutputSignal OnStart;

        private void Start()
        {
            OnStart.Invoke();
        }
    }
}