using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace Krem.AppCore.Basis.Components.Handlers
{
    [NodeGraphGroupName("Jet Pack/Basis/Handlers")]
    public class EnabledHandler : CoreComponent
    {
        [Header("Ports")]
        public OutputSignal OnEnabled;

        private void OnEnable()
        {
            OnEnabled.Invoke();
        }
    }
}