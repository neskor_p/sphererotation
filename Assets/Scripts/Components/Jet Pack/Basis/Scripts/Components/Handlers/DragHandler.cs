using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Krem.AppCore.Basis.Components.Handlers
{
    [NodeGraphGroupName("Jet Pack/Basis/Handlers")]
    public class DragHandler : CoreComponent, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [Header("Ports")]
        public OutputSignal OnStartDragging;
        public OutputSignal OnDragging;
        public OutputSignal OnEndDragging;
        
        private PointerEventData _pointerEventData;
        private Vector2 _startPosition;
        private Vector2 _endPosition;
        
        public PointerEventData PointerEventData => _pointerEventData;
        public Vector2 StartPosition => _startPosition;
        public Vector2 EndPosition => _endPosition;
        public bool IsDragging { get; set; } = false;

        public void OnBeginDrag(PointerEventData eventData)
        {
            IsDragging = true;
            _pointerEventData = eventData;
            _startPosition = eventData.position;
            
            OnStartDragging.Invoke();
        }

        public void OnDrag(PointerEventData eventData)
        {
            _pointerEventData = eventData;
            
            OnDragging.Invoke();
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            IsDragging = false;
            _pointerEventData = eventData;
            _endPosition = eventData.position;
            
            OnEndDragging.Invoke();
        }
    }
}