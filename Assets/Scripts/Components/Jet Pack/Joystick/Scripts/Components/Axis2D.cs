using Krem.AppCore.Attributes;
using Krem.AppCore.Joystick.Interfaces;
using UnityEngine;

namespace Krem.AppCore.Joystick.Components
{
    [NodeGraphGroupName("Jet Pack/Joystick")]
    [DisallowMultipleComponent]
    public abstract class Axis2D : CoreComponent, IAxis2D
    {
        public virtual Vector2 Axis { get; set; } = Vector2.zero;
    }
}