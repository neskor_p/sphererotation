using Krem.AppCore.Attributes;

namespace Krem.AppCore.Joystick.Actions
{
    [NodeGraphGroupName("Jet Pack/Joystick")]
    public class ShowBody : CoreAction
    {
        [InjectComponent] private Components.Joystick _joystick;
        
        protected override bool Action()
        {
            _joystick.Body.gameObject.SetActive(true);
        
            return true;
        }
    }
}