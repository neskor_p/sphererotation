using Krem.AppCore.Attributes;

namespace Krem.AppCore.Joystick.Actions
{
    [NodeGraphGroupName("Jet Pack/Joystick")]
    public class ShowHandle : CoreAction
    {
        [InjectComponent] private Components.Joystick _joystick;
        
        protected override bool Action()
        {
            _joystick.Handle.gameObject.SetActive(true);
        
            return true;
        }
    }
}