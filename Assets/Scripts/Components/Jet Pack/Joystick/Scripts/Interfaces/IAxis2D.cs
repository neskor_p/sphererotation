using UnityEngine;

namespace Krem.AppCore.Joystick.Interfaces
{
    public interface IAxis2D
    {
        public Vector2 Axis { get; set; }
    }
}
