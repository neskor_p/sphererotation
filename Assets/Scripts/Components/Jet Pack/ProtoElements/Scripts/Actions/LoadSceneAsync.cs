using System.Collections;
using Krem.AppCore.Attributes;
using Krem.AppCore.Basis.Components;
using Krem.AppCore.ProtoElements.Components;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Krem.AppCore.ProtoElements.Actions
{
    [NodeGraphGroupName("Jet Pack/Proto Elements")]
    public class LoadSceneAsync : CoreAction
    {
        [InjectComponent] private CoroutineComponent _coroutineComponent;
        [InjectComponent] private AsyncSceneLoader _asyncSceneLoader;

        private bool _loadingState = false;

        protected override bool Action()
        {
            if (_loadingState)
            {
                return false;
            }

            _loadingState = true;
            
            _coroutineComponent.StartCoroutine(LoadYourAsyncScene());
            
            return true;
        }

        private IEnumerator LoadYourAsyncScene()
        {

            Scene currentScene = SceneManager.GetActiveScene();
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(
                _asyncSceneLoader.SceneComponent.reloadCurrentScene ? 
                    currentScene.name : 
                    _asyncSceneLoader.SceneComponent.sceneName
                    );

            while (asyncLoad.isDone == false)
            {
                _asyncSceneLoader.HorizontalProgressBar.ProgressValue = asyncLoad.progress;
                yield return null;
            }

            _loadingState = false;
            _asyncSceneLoader.HorizontalProgressBar.ProgressValue = 1f;
        }
    }
}