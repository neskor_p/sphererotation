using Krem.AppCore.Attributes;
using UnityEngine;

namespace Krem.AppCore.EventBus.Components
{
    [NodeGraphGroupName("Jet Pack/Event Bus")]
    [DisallowMultipleComponent]
    public class EventBusInvoker : BaseEventBusInvoker<ScriptableEventBus>
    {    
    
    }
}