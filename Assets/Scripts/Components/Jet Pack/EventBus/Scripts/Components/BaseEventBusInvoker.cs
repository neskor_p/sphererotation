using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace Krem.AppCore.EventBus.Components
{
    public abstract class BaseEventBusInvoker<T> : CoreComponent where T : ScriptableEventBus
    {    
        [Header("Dependencies")]
        [SerializeField] private T EventBus;

        [Header("Ports")]
        [BindInputSignal(nameof(InvokeEvent))] public InputSignal CallInvoke;
        public OutputSignal OnInvoke;

        public virtual void InvokeEvent()
        {
            EventBus.Invoke();
            
            OnInvoke.Invoke();
        }
    }
}