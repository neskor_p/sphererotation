using Krem.AppCore.Ports;
using UnityEngine;

namespace Krem.AppCore.EventBus.Components
{
    public abstract class BaseEventBusListener<T> : CoreComponent where T : ScriptableEventBus
    {    
        [Header("Dependencies")]
        [SerializeField] private T EventBus;

        [Header("Ports")]
        public OutputSignal OnHandle;

        private void OnEnable()
        {
            EventBus.AddListener(Handler);
        }

        private void OnDisable()
        {
            EventBus.RemoveListener(Handler);
        }

        protected virtual void Handler()
        {
            OnHandle.Invoke();
        }
    }
}